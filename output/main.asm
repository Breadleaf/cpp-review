bits 64

section .text
	global _start

_start:
	; Open file
	mov rax, 2     ; sys_open
	mov rdi, file  ; (string with file name)
	mov rsi, 64+2  ; Create + Write
	mov rdx, 0644o ; File permissions
	syscall

	; Print to file
	mov rdi, rax     ; (open file stream)
	mov rax, 1       ; sys_open
	mov rsi, msg     ; (string with text)
	mov rdx, msg_len ; (int with length of string)
	syscall

	; Print to stdout
	mov rax, 1       ; sys_write
	mov rdi, 1       ; stdout
	mov rsi, msg     ; (string with text)
	mov rdx, msg_len ; (int with length of string)
	syscall

	; Exit program
	mov rax, 60 ; sys_exit
	mov rdi, 0  ; (int exit code)
	syscall

section .rodata
msg db "Hello World", 10
msg_len equ $ - msg

file db "test.txt", 0
