#include <iostream>
#include <fstream>
#include <string>

int main()
{
	std::string message = "Hello World\n";

	std::ofstream file("test.txt"); // open file stream

	if (file.fail())
	{
		// std::cerr prints to stderr
		std::cerr << "Error with file." << std::endl;
		exit(1);
	}

	file << message; // write text to file

	file.close(); // close file when done

	std::cout << message; // std::cout prints to stdout

	return 0;
}
