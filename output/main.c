#include <stdio.h>

int main()
{
	FILE *fp = fopen("test.txt", "w");
	const char *message = "Hello World\n";

	fprintf(fp, "%s", message);     // print to a file (fp)
	fprintf(stdout, "%s", message); // printing to a file (stdout)
	printf("%s", message);          // print to stdout

	return 0;
}
