#include <iostream>
using namespace std;

template <class T, class H>
struct storage
{
	T store_1;
	H store_2;
};

int main()
{
	storage<int, char> sic;
	sic.store_1 = 10;
	sic.store_2 = 'a';
	return 0;
}
