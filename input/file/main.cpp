#include <iostream>
#include <fstream>
#include <vector>
#include <string>
using namespace std;

struct person
{
	string first_name, last_name;
	char middle_initial;
	unsigned int age;

	bool is_21() { return (age >= 21 ? true : false); }
};

int main()
{
	string file_name = "file.txt";
	vector<person> vp;

	ifstream file(file_name);

	if (file.fail())
	{
		cerr << "Error opening file: " << file_name << endl;
		exit(1);
	}

	while (!file.eof())
	{
		person temp;
		file >> temp.first_name;
		file >> temp.middle_initial;
		file >> temp.last_name;
		file >> temp.age;
		vp.push_back(temp);
	}
	vp.pop_back();

	file.close();

	for (person p: vp)
	{
		cout << "Hi " << p.first_name << " "
			<< p.middle_initial << " " << p.last_name
			<< "! You are " << p.age << " years old. You can"
			<< (p.is_21() ? "" : "not") << " drink.\n";
	}

	return 0;
}
