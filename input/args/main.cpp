#include <iostream>
using namespace std;

int main(int argc, char **argv)
{
	for (size_t i = 0; i < argc; i++)
		cout << i << " -> " << argv[i] << endl;
	return 0;
}
