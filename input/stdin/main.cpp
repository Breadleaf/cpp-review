#include <iostream>
#include <limits>
using namespace std;

// Function declarations
int get_num();
bool is_op(char c);
char get_operator();
bool is_zero(int i);

int main()
{
	int x(get_num()), y(get_num());
	double z;
	char op(get_operator());

	cout << x << " " << op << " " << y << " = ";

	switch (op)
	{
		case '+':
			z = x + y;
			break;
		case '-':
			z = x - y;
			break;
		case '*':
			z = x * y;
			break;
		case '/':
			if (y == 0)
			{
				cout << "Divide by zero error!\n";
				exit(1);
			}

			z = (double)x / y;
			break;
	}

	cout << z << endl;

	return 0;
}

// Function definitions
int get_num()
{
	int tmp;

	while (true)
	{
		cout << "Enter a number: ";
		cin >> tmp;

		if (cin.fail())
		{
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			cout << "Invalid input\n";
		} else {
			break;
		}
	}

	return tmp;
}

bool is_op(char c)
{
	switch (c)
	{
		case '+':
		case '-':
		case '*':
		case '/':
			return true;
		default:
			return false;
	}
}

char get_operator()
{
	char tmp;
	
	while (true)
	{
		cout << "operators: + - * /" << endl;
		cout << "Enter a operator: ";
		cin >> tmp;

		if (cin.fail())
		{
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			cout << "Invalid input\n";
		} else {
			if (is_op(tmp))
			{
				break;
			}
			else
			{
				cout << "Invalid input\n";
			}
		}
	}

	return tmp;
}
