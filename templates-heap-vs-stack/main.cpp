#include <iostream>
#include <vector>
using namespace std;

template <class T>
class storage
{
	T _data;

	public:
	storage(T value) : _data(value) {}
	void print() { cout << this->_data << endl; }
};

int main()
{
	storage<int> s(5); // Stack
	storage<int> *s2 = new storage<int>(10); // Heap

	s.print();
	s2->print();

	vector<int> vi = {1,2,3,4,5};

	for (auto a: vi) { cout << a << endl; }

	delete s2;
	return 0;
}
