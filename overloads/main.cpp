#include <iostream>
#include <vector>
using namespace std;

template <class T>
ostream& operator<<(ostream& os, const vector<T> vt);

int main()
{
	vector<int> vi = {1,2,3,4,5};
	cout << vi << endl;
	return 0;
}

template <class T>
ostream& operator<<(ostream& os, const vector<T> vt)
{
	os << "[";

	size_t iter;
	for (iter = 0; iter < vt.size() - 1; iter++)
		os << vt[iter] << ", ";

	os << vt[iter] << "]";

	return os;
}
